# Dasar-Dasar Perancangan dan Pemrograman Web (DDPPW)

*Pengenalan sederhana terhadap perancangan dan pemrograman situs web dengan framework Django*

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-sa/4.0/) 

## Daftar Isi

* Pengenalan
* Persyaratan
* Daftar Materi
* *Acknowledgements*
* Lisensi

## Pengenalan

**Dasar-Dasar Perancangan dan Pemrograman Web** merupakan dokumen materi yang disusun untuk memberikan pengenalan terhadap perancangan dan pemrograman situs web menggunakan *framework* Django. Materi ini disusun sebagai penunjang kegiatan dengan nama yang sama.

## Persyaratan

Materi ini mengasumsikan bahwa pembaca sudah memahami pemrogrmaan dengan menggunakan Bahasa Python 3.

Daftar aplikasi yang dibutuhkan untuk mengikuti alur materi ini tersedia di materi Pekan 0.

## Daftar Materi

Materi yang disajikan pada repositori ini dibangun berdasarkan Buku Rancangan Pengajaran Mata Kuliah Perancangan dan Pemrograman Web (CSGE602022) edisi Semester Genap 2018/2019 di Fasilkom UI. Susunan materi dimodifikasi untuk meningkatkan porsi pembelajaran secara praktik. Adapun daftar materi DDPPW adalah sebagai berikut:

1. Pekan 0: Pengenalan Perangkat Lunak
2. Pekan 1: HTML dan CSS Dasar
3. Pekan 2: Peluncuran Situs Pertama dengan Django
4. Pekan 3: Pengenalan *Templating* Django

## *Acknowledgements*

Pembuatan materi ini tidak terlepas dari bantuan pihak-pihak yang berkontribusi baik secara langsung maupun tidak langsung, seperti:

1. Tim Dosen dan Asisten Dosen Perancangan dan Pemrograman Web Semester Genap 2018/2019 Fasilkom UI
2. Kontributor Repositori GitLab Kuliah PPW Tahun 2017 ([ppw-lab](https://gitlab.com/PPW-2017/ppw-lab))

Referensi yang digunakan untuk materi didaftarkan pada masing-masing pekan materi.

## Lisensi

[![License: CC BY-SA 4.0](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Penggunaan dan modifikasi terhadap repositori materi ini disesuaikan dengan ketentuan yang tertera pada lisensi **Creative Commons Attribution-ShareAlike 4.0 International**.

Kontribusi dapat disampaikan melalui kolom **Issues** pada repositori ini atau melalui pos elektronik ke alamat [rafi.daffa@gmail.com](mailto:rafi.daffa@gmail.com).