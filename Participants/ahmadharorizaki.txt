Ahmad Harori Zaki Ichsan
ahmadharorizaki

https://gitlab.com/ahmadharorizaki/DDPPW

Checklist Week00

GIT - PRIBADI
[X] Membuat repositori Git di lokal dengan menggunakan git init
[X] Membuat suatu file dan menambahkannya ke dalam staging repositori
[X] Melakukan commit terhadap perubahan di staging dengan git commit
[X] Menambahkan remote baru ke repositori lokal dengan git init
[X] Melakukan push ke GitLab
[X] Membuat satu branch di repositori dan push branch tersebut dengan isi 1 file
[X] Membuat merge request ke branch master dan melakukan merge
[X] Mengetahui cara menggali informasi tentang commit melalui git log
[X] Mengganti level akses GitLab ke Public

GIT - BERSAMA
[X] Melakukan git init, git remote add, dan git pull terhadap repositori ini (https://gitlab.com/skycruiser8/ddppw)
[X] Membuat branch baru dengan nama yang unik (boleh nama sendiri, nama mantan, dll.)
[X] Membuat berkas <username gitlab>.txt di folder Participants dengan konten seperti berkas ini
[X] Push branch masing-masing ke GitLab
[X] Membuat merge request ke branch master

VIRTUALENV
[ ] Membuat virtual environment baru di repositori DDPPW masing-masing
[ ] Menambahkan folder virtual environment ke .gitignore
[ ] Memastikan bahwa virtual environment tidak turut terdorong ke GitLab
[ ] Menjalankan virtual environment
[ ] Memasang Django di virtual environment
[ ] Memasang paket aplikasi DDPPW dengan menggunakan requirements.txt